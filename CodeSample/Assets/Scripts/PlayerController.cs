using UnityEngine;

namespace CodeSample2.Player
{
    public class PlayerController : MonoBehaviour
    {
        private void OnEnable()
        {
            GameManager.Instance.Inputs.Enable();
        }

        private void OnDisable()
        {
            GameManager.Instance.Inputs.Disable();
        }
    }
}