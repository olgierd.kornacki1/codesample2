using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CodeSample2.Menu.Settings
{
    public class Keybind : MonoBehaviour
    {
        public InputActionReference targetAction;

        [SerializeField] private UnityEngine.UI.Button button = null;
        [SerializeField] private TMP_Text buttonText = null;

        [Header("Change if action have more then one binding")]
        [SerializeField] private int index = 0;

        private ControlsMenu menu;

        public void SetMenu(ControlsMenu _menu) => menu = _menu;

        private void Start() => SetButtonText();

        public void StartRebiding()
        {
            if (menu.rebindingOperation != null) return;

            InputAction _inputAction = GameManager.Instance.Inputs.asset[targetAction.name];
            buttonText.text = "assign...";
            button.interactable = false;
            menu.rebindingOperation = _inputAction.PerformInteractiveRebinding()
                 .WithTargetBinding(index)
                 .OnMatchWaitForAnother(0.1f)
                 .OnComplete(operation => RebindComplete())
                 .Start();
        }

        private void RebindComplete()
        {
            InputAction _inputAction = GameManager.Instance.Inputs.asset[targetAction.name];
            SaveBindingOverride(_inputAction);

            menu.rebindingOperation.Dispose();
            menu.rebindingOperation = null;
            button.interactable = true;

            SetButtonText();
        }

        public void SetButtonText() => buttonText.text = GameManager.Instance.Inputs.asset[targetAction.name].GetBindingDisplayString(index).ToUpper();

        public void SaveBindingOverride(InputAction action) => PlayerPrefs.SetString(action.actionMap + action.name + index, action.bindings[index].overridePath);
    }
}