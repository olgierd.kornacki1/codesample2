using CodeSample2.Interactable;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CodeSample2.Player
{
    public class PlayerInteractionController : MonoBehaviour
    {
        [SerializeField] private TMPro.TextMeshProUGUI text;

        private bool canInteract = false;

        [Header("Interaction Raycast")]
        [SerializeField] private float range;

        [SerializeField] private LayerMask layer;

        private IInteraction objectDetected;

        private void Start() => GameManager.Instance.Inputs.PlayerActions.Interaction.performed += _ => Interaction();

        private void Update()
        {
            if (Physics.Raycast(transform.position, transform.right, out RaycastHit hit, range, layer))
            {
                objectDetected = hit.collider.GetComponent<IInteraction>();

                if (objectDetected != null && objectDetected.CanInteract) InteractionState(true, "Press " + GameManager.Instance.Inputs.PlayerActions.Interaction.GetBindingDisplayString().ToUpper() + " to " + objectDetected.InteractionText);
                else InteractionState(false, "");
            }
            else InteractionState(false, "");
        }

        private void Interaction()
        {
            if (canInteract) objectDetected.Interaction();
        }

        private void InteractionState(bool playerInRange, string interactionText)
        {
            canInteract = playerInRange;
            text.text = interactionText;
        }
    }
}