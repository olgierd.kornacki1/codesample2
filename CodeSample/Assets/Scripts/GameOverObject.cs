using UnityEngine;

namespace CodeSample2.Gameplay
{
    public class GameOverObject : MonoBehaviour, Interactable.IInteraction
    {
        public bool CanInteract
        { get => true; set { } }

        public void Interaction()
        {
            GameManager.Instance.LoadMenu();
        }

        public string InteractionText => "exit game";
    }
}