using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CodeSample2.Menu.Settings
{
    public class ControlsMenu : MonoBehaviour
    {
        public InputActionRebindingExtensions.RebindingOperation rebindingOperation;

        [SerializeField] private List<Keybind> keybinds = new();

        private void Start()
        {
            SetKeybinds();
        }
        private void SetKeybinds()
        {
            foreach (Keybind keybind in keybinds)
            {
                keybind.SetMenu(this);
            }
        }

        public void ResetKeybindOverriddes()
        {
            Debug.Log("reset");
            foreach (InputAction action in GameManager.Instance.Inputs.asset) action.RemoveAllBindingOverrides();
            foreach (Keybind keybind in keybinds)
            {
                keybind.SetButtonText();
                keybind.SaveBindingOverride(keybind.targetAction);
            }
        }
    }
}