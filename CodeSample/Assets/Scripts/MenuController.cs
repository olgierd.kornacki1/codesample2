using UnityEngine;

namespace CodeSample2.Menu
{
    public class MenuController : MonoBehaviour
    {
        [SerializeField] private Transform MainMenu;
        [SerializeField] private Transform ControlsMenu;

        public void ChangeMenuState(bool openControls)
        {
            MainMenu.gameObject.SetActive(openControls);
            ControlsMenu.gameObject.SetActive(!openControls);
        }

        public void ExitGame()
        {
            GameManager.Instance.CloseApplication();
        }

        public void StartGame()
        {
            GameManager.Instance.LoadGame();
        }
    }
}