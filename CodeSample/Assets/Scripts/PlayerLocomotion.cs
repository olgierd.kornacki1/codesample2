using UnityEngine;

namespace CodeSample2.Player
{
    public class PlayerLocomotion : MonoBehaviour
    {
        [SerializeField] private float maxSpeed = 5f;
        [SerializeField] private float acceleration = 5f;

        private PlayerControls controls;
        private CharacterController characterController;

        private Vector2 movementInput;
        private Vector3 currentVelocity;

        private void Awake()
        {
            controls = GameManager.Instance.Inputs;
            characterController = GetComponent<CharacterController>();
        }

        private void Update()
        {
            HandleMovement();
        }

        private void HandleMovement()
        {
            movementInput = controls.PlayerActions.Movement.ReadValue<Vector2>();

            float desiredVelocity = movementInput.x * maxSpeed;
            currentVelocity.x = Mathf.MoveTowards(currentVelocity.x, desiredVelocity, acceleration * Time.deltaTime);

            characterController.Move(currentVelocity * Time.deltaTime);
        }
    }
}