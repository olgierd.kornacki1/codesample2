using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace CodeSample2
{
    public class GameManager : MonoBehaviour
    {
        private static GameManager instance = null;

        public static GameManager Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject singletonObject = new("GameManager");
                    instance = singletonObject.AddComponent<GameManager>();
                }

                return instance;
            }
        }

        public PlayerControls Inputs;

        private void Awake()
        {
            InitializeSingleton();
            Inputs = new PlayerControls();
        }

        private void Start()
        {
            LoadMenu();
            LoadBindingsOverrides();
        }

        public void LoadMenu()
        {
            SceneManager.LoadScene("MenuScene");
        }

        public void LoadGame()
        {
            SceneManager.LoadScene("GameScene");
        }

        public void CloseApplication()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
        }

        private void LoadBindingsOverrides()
        {
            foreach (InputAction action in Inputs.asset)
            {
                for (int i = 0; i < action.bindings.Count; i++)
                {
                    if (!string.IsNullOrEmpty(PlayerPrefs.GetString(action.actionMap + action.name + i)))
                        action.ApplyBindingOverride(i, PlayerPrefs.GetString(action.actionMap + action.name + i));
                }
            }
        }

        private void InitializeSingleton()
        {
            if (instance != null && instance != this)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                DontDestroyOnLoad(gameObject);
            }
        }
    }
}