namespace CodeSample2.Interactable
{
    public interface IInteraction
    {
        public bool CanInteract { get; set; }

        public void Interaction() { }

        public string InteractionText { get; }
    }
}